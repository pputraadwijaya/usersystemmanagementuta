<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', 'UserManagemenController@index');
Route::post('/user/store', 'UserManagemenController@store');
Route::get('/user/edit/{id}', 'UserManagemenController@edit');
Route::put('/user/update/{id}', 'UserManagemenController@update');
Route::delete('/user/delete/{id}', 'UserManagemenController@destroy'); 

